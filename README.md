# Frontend interview Questions on React


## How to start
1. clone repo
2. run npm i in the folder
3. run npm start

## Questions

### Question 1
This question is about how the user would organize their components in react
What components can be made from the dashboard? 
What can be a reusable component? 
What is the difference between functional components vs class components?
Which components would you make a functional component?

![picture](https://bitbucket.org/kevncheng/frontend-interview-questions/raw/master/question1.png)


### Question 2
This question is about the importance of the key prop when you programatically render a collection of child components.
What are the probable causes, how would they start to debug it.
The purpose to see if they can debug on their own and understand a fundamental react's diffing algorithm.

When setting the option of the item, why does the option gets transferred to another item when I delete the row.

![picture](https://bitbucket.org/kevncheng/frontend-interview-questions/raw/master/question2.png)

A. Keys help React identify which items have changed, are added, or are removed.
Keys should be given to the elements inside the array to give the elements a stable identity.
React needs to figure out what are the differences to efficiently update the UI
React will mutate every child instead of realizing it can keep items 1, 2 and 3 intact.

### Question 3
This question is another debuging questions and how react update the states.
There is a sign up form and you want a helper text to appear when a field is left blank.

When you press sign up why does only one error appear and not the other? How would you fix it?

![picture](https://bitbucket.org/kevncheng/frontend-interview-questions/raw/master/question3code.png) ![picture](https://bitbucket.org/kevncheng/frontend-interview-questions/raw/master/question3.png)

A. Combine the updates into one update so that setstate is only being called once. React setState is async , when you try to update state multiple times in a handler, react will wait for the event to finish before re-rendering.