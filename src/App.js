import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import SignUp from './Page/SignUp';
import EmployeeQuestion from './Page/EmployeeQuestion';
import QuestionPage from './Page/QuestionPage';
import ComponentQuestion from './Page/ComponentQuestion';

function App() {
    return (
        <Router>
            <Route exact path='/' component={QuestionPage} />
            <Route exact path='/question1' component={ComponentQuestion} />
            <Route path='/question2' component={EmployeeQuestion} />
            <Route path='/question3' component={SignUp} />
        </Router>
    );
}

export default App;
