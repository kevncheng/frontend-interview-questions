import React, { Component } from 'react';
import dashboardpage from '../image/component.png';
import { Container } from '@material-ui/core';
export default class ComponentQuestion extends Component {
    render() {
        return (
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <h2>How would you organized the components?</h2>
                <img
                    src={dashboardpage}
                    alt='component'
                    style={{ height: '50%', width: '50%' }}
                />
                <a href='/question2'>Question 2 ></a>
            </div>
        );
    }
}
