import React from 'react';
import './styles.css';

export default function EmployeeQuestion() {
    const [employees, setStatus] = React.useState([
        {
            id: 1,
            name: 'Kyle',
            lname: 'Smith',
            occupation: 'Student'
        },
        { id: 2, name: 'Jane', lname: 'Doe', occupation: 'UI/UX Designer' },
        {
            id: 3,
            name: 'Kelly',
            lname: 'Romaro',
            occupation: 'Software Developer'
        },
        { id: 4, name: 'Jacob', lname: 'Santos', occupation: 'Influencer' },
        { id: 5, name: 'Michael', lname: 'Page', occupation: 'Designer' },
        {
            id: 6,
            name: 'Ryan',
            lname: 'Patterson',
            occupation: 'Fullstack Developer'
        }
    ]);

    // Takes in the id of the employee
    // and removes the employee from the array.
    // Then set the state to the new filtered array
    const deleteEmployee = employeeId => {
        const updatedEmployee = employees.filter(
            employee => employee.id !== employeeId
        );
        setStatus(updatedEmployee);
    };

    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <h1 style={{ textAlign: 'center' }}>Rate these employees!</h1>
            {employees.map((employee, i) => (
                <li
                    style={{
                        justifyContent: 'center',
                        display: 'flex',
                        alignItems: 'center'
                    }}
                >
                    {`${employee.name} ${employee.lname} | ${employee.occupation}`}
                    <select>
                        <option>Bad</option>
                        <option>Okay</option>
                        <option>Very good</option>
                    </select>
                    <button onClick={() => deleteEmployee(employee.id)}>
                        Fire
                    </button>
                </li>
            ))}
            <a style={{ textAlign: 'center' }} href='/question3'>
                Question 3 >
            </a>
        </div>
    );
}

// Keys help React identify which items have changed, are added, or are removed.
// Keys should be given to the elements inside the array to give the elements a stable identity.
// React needs to figure out what are the differences to efficiently update the UI
// React will mutate every child instead of realizing it can keep items 1, 2 and 3 intact.
