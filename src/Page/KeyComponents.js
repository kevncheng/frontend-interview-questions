import React, { Component } from 'react';
import { Button, Container } from '@material-ui/core';
import _ from 'lodash';

const data = {
    data1: [
        {
            id: 1,
            name: 'Kyle',
            lname: 'Smith',
            occupation: 'Student'
        },
        { id: 2, name: 'Jane', lname: 'Doe', occupation: 'UI/UX Designer' },
        {
            id: 3,
            name: 'Kelly',
            lname: 'Romaro',
            occupation: 'Software Developer'
        }
    ],
    data2: [
        {
            id: 1,
            name: 'Jacob',
            lname: 'Santos',
            occupation: 'Influencer'
        },
        { id: 2, name: 'Michael', lname: 'Page', occupation: 'Designer' },
        {
            id: 3,
            name: 'Ryan',
            lname: 'Patterson',
            occupation: 'Fullstack Developer'
        }
    ]
};

export default class KeyComponents extends Component {
    state = {
        selected: 'data1'
    };

    onBtnClick = value => {
        this.setState({ selected: value });
    };

    render() {
        return (
            <Container>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <Button
                        variant='contained'
                        color={
                            this.state.selected === 'data1'
                                ? 'primary'
                                : 'secondary'
                        }
                        onClick={() => this.onBtnClick('data1')}
                    >
                        Data 1
                    </Button>
                    <Button
                        variant='contained'
                        color={
                            this.state.selected === 'data2'
                                ? 'primary'
                                : 'secondary'
                        }
                        onClick={() => this.onBtnClick('data2')}
                    >
                        Data 2
                    </Button>
                </div>
                <div>
                    {_.map(data[this.state.selected], item => (
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'center'
                            }}
                        >
                            <li>
                                <div>{`${item.name} ${item.lname} | `}</div>
                                <div>{item.occupation}</div>
                            </li>
                        </div>
                    ))}
                </div>
            </Container>
        );
    }
}
