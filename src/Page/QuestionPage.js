import React, { Component } from 'react';
import { Container } from '@material-ui/core';

export default class QuestionPage extends Component {
    render() {
        return (
            <Container
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '70vh'
                }}
            >
                <a href='/question1'>Question 1</a>
                <a href='/question2'>Question 2</a>
                <a href='/question3'>Question 3</a>
            </Container>
        );
    }
}
