import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}));

export default function SignUp() {
    const classes = useStyles();
    const [inputs, setInputs] = useState({
        fname: '',
        lname: '',
        email: '',
        password: ''
    });
    const [errors, setErrors] = useState({});
    const { fname, lname, email, password } = inputs;
    const onChangeText = e => {
        setInputs({ ...inputs, [e.target.name]: e.target.value });
    };

    // Question
    // What would you expect to happen if you click sign up?
    // How would you improve this function?
    const onSignUpPressed = () => {
        const message = 'Please do not leave empty';
        if (fname.length <= 0) {
            setErrors({ ...errors, fname: message });
        }
        if (lname.length <= 0) {
            setErrors({ ...errors, lname: message });
        }
        if (email.length <= 0) {
            setErrors({ ...errors, email: message });
        }
        if (password.length <= 0) {
            setErrors({ ...errors, password: message });
        }
    };

    return (
        <Container component='main' maxWidth='xs'>
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component='h1' variant='h5'>
                    Sign up
                </Typography>
                <div className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete='fname'
                                name='fname'
                                variant='outlined'
                                required
                                fullWidth
                                id='firstName'
                                label='First Name'
                                error={Boolean(errors.fname)}
                                helperText={errors.fname}
                                value={inputs.fname}
                                onChange={e => onChangeText(e)}
                                autoFocus
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant='outlined'
                                required
                                fullWidth
                                id='lastName'
                                label='Last Name'
                                error={Boolean(errors.lname)}
                                helperText={errors.lname}
                                value={inputs.lname}
                                onChange={e => onChangeText(e)}
                                name='lname'
                                autoComplete='lname'
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant='outlined'
                                required
                                fullWidth
                                id='email'
                                label='Email Address'
                                error={Boolean(errors.email)}
                                helperText={errors.email}
                                value={inputs.email}
                                onChange={e => onChangeText(e)}
                                name='email'
                                autoComplete='email'
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant='outlined'
                                required
                                fullWidth
                                name='password'
                                label='Password'
                                error={Boolean(errors.password)}
                                helperText={errors.password}
                                value={inputs.password}
                                onChange={e => onChangeText(e)}
                                type='password'
                                id='password'
                                autoComplete='current-password'
                            />
                        </Grid>
                    </Grid>
                    <Button
                        fullWidth
                        variant='contained'
                        color='primary'
                        className={classes.submit}
                        onClick={() => onSignUpPressed()}
                    >
                        Sign Up
                    </Button>
                    <Grid container justify='flex-end'>
                        <Grid item>
                            <Link href='#' variant='body2'>
                                Already have an account? Sign in
                            </Link>
                        </Grid>
                    </Grid>
                </div>
            </div>
        </Container>
    );
}

// Answer to fix
// Put the updates in a single object and then update the state only once
// const applyErrors = _.reduce(
//     inputs,
//     (acc, val, key) => {
//         if (val.length <= 0) {
//             acc[key] = message;
//         }
//         return acc;
//     },
//     {}
// );
// setErrors(applyErrors);

// React setState is async , when you try to update state multiple times in a handler,
// react will wait for the event to finish before re-rendering.
